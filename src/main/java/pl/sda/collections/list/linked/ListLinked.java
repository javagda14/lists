package pl.sda.collections.list.linked;

import java.util.List;

public class ListLinked {

    private ListElement head;
    private ListElement tail;

    private int size;

    public ListLinked() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    // add(Object o)

    public void add(Object dane) {
        ListElement element = new ListElement(dane);

        if (size == 0 && head == null && tail == null) {
            // lista jest pusta

            this.head = element;
            this.tail = element;
        } else if (size == 1 && head == tail) {
            // jest tylko jeden element
            this.tail = element;

            this.head.setNext(element); // head Twój następnik to element
            element.setPrev(this.head); // element Twój poprzednik to head
        } else {
            // dodaje gdzieś na koniec nowy element
            this.tail.setNext(element);
            element.setPrev(this.tail);

            this.tail = element;
        }

        this.size++;
    }

    // remove(int index)
    public void remove(int index) {
//        if(index == 0){
//            removeFirst();
//            return;
//        }

        ListElement tmp = this.head;

        int count = 0;
        while (tmp != null && count != index) {
            tmp = tmp.getNext(); // przejście do następnego elementu
            count++;
        }

        if (tmp == null) {
            throw new ArrayIndexOutOfBoundsException("out of bounds");
        }

        ListElement poprzednik = tmp.getPrev();
        ListElement następnik = tmp.getNext();

        if (poprzednik != null) {
            poprzednik.setNext(następnik);
        } else {
            head = head.getNext();// jeśli usuwam z indexu 0
        }

        if (następnik != null) {
            następnik.setPrev(poprzednik);
        } else {
            tail = tail.getPrev();// jeśli usuwam z indexu ostatniego
        }

        this.size--;
    }

    public void removeLast() {
        // ostatni to teraz przedostatni
        this.tail = this.tail.getPrev();

        // następnikiem ostatniego jest null
        this.tail.setNext(null);
        this.size--;
    }

    public void removeFirst() {
        // pierwszym elementem staje się drugi element
        this.head = this.head.getNext();

        // poprzednikiem pierwszego elementu jest null
        this.head.setPrev(null);
        this.size--;
    }

    public Object get(int index) {
        ListElement tmp = this.head;

        int count = 0;
        // wykonujemy przejście po elementach listy
        while (tmp != null && index != count) {
            tmp = tmp.getNext(); // przejście do następnego elementu
            count++;
        }

        // jeśli dotarłem za daleko lub nie odnalazłem szukanego elementu
        if (tmp == null || index != count) {
            return null;
        }

        // jeśli odnalazłem szukany węzeł (indeks) to zwracam jego wartość
        return tmp.getValue();
    }

    public void clear() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    public int indexOf(Object o) {
        ListElement tmp = this.head;

        int index = 0;
        // wykonujemy przejście po elementach listy
        while (tmp != null && tmp.getValue() != o) {
            tmp = tmp.getNext(); // przejście do następnego elementu
            index++;
        }

        // jeśli nie uda nam się znaleźć elementu
        if (tmp == null || tmp.getValue() != o) {
            return -1;
        }

        return index;
    }

    private String toStringTab() {
        StringBuilder builder = new StringBuilder();

        ListElement tmp = this.head;

        while (tmp != null) {
            builder.append(tmp.getValue() + " ");
            tmp = tmp.getNext(); // przejście do następnego elementu
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        return "ListLinked{" +
                "data=" + toStringTab() +
                '}';
    }
}
