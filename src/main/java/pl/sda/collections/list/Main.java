package pl.sda.collections.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();

        long timeStart = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }

        System.out.println(System.currentTimeMillis() - timeStart);


        timeStart = System.currentTimeMillis();
//        for (int i = 0; i < list.size(); i++) { // suboptymalna  - n kwadrat
//            list.get(i);
//        }
        for (Integer wartosc : list) {          // optymalna - złożoność n
            wartosc++;
        }
        System.out.println(System.currentTimeMillis() - timeStart);
    }
}
