package pl.sda.collections.list.array;

public class Main {
    public static void main(String[] args) {
        ListArray listArray = new ListArray();

        listArray.add(5);
        listArray.add(3);
        listArray.add("ajwbdhajwfbajwjfb");
        listArray.add(6);
        listArray.add("Paweł");
        listArray.add(2);
        listArray.add(1);
        listArray.add(10);

        System.out.println(listArray);
        listArray.remove(0);

        System.out.println("------");
        System.out.println(listArray);

        listArray.remove(1);
        System.out.println("------");
        System.out.println(listArray);

//        listArray.remove(-20);
//        System.out.println("------");
//        System.out.println(listArray);

        //

        System.out.println("IndexOf: " + listArray.indexOf("Paweł"));

        System.out.println(listArray.size());
        System.out.println(listArray.contains("Paweł"));

        listArray.add(1, 1, 2, 3, 4, 4, 4, 4);
        System.out.println(listArray);
    }
}
