package pl.sda.collections.list.array;

import java.util.Arrays;

public class ListArray {

    private Object[] tab;
    private int size = 0;

    public ListArray() {
        this.tab = new Object[5];
    }

    public ListArray(int iloscElementow) {
        this.tab = new Object[iloscElementow];
    }

    public void add(Object object) {
        // jeśli jest za mało miejsca, rozszerz tablicę
        if (this.size >= this.tab.length) {
            rozszerzTablicę();
        }

        // dopisuje nowy element
        // wstawiamy do tablicy o indeksie [obecny_rozmiar]
        this.tab[size++] = object;
        // po
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "tab=" + toStringTab() +
                ", size=" + size +
                '}';
    }

    /**
     * Wstawienie elementu 'object' na indeks 'index'.
     *
     * @param index
     * @param object
     */
    public void add(int index, Object object) {
        // todo: implementacja
    }

    private String toStringTab() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.size; i++) {
            builder.append(this.tab[i] + ", ");
        }
        return builder.toString();
    }

    public void remove(int indeks) {
        // iterujemy tablicę od elementu o indeksie wskazanym do usunięcia
        // aż do końca listy (do rozmiaru listy)
        for (int i = indeks; i < this.size; i++) {
            // wewnątrz pętli
            // przesuwamy element z indeksu o 1 wyższego do elementu aktualnego
            // tab[indeks] = tab[indeks+1];
            this.tab[i] = this.tab[i + 1];
        }

        // redukujemy rozmiar listy i wstawiamy null na końcu (opcjonalne)
        this.size--;
        this.tab[this.size] = null;
    }

    private void rozszerzTablicę() {
        // brakuje nam miejsca w tablicy
        // kopiujemy elmenty z tablicy
        Object[] kopia = new Object[this.tab.length * 2];
        // przepisuje elementy ze starej tablicy do nowej
        for (int i = 0; i < this.tab.length; i++) {
            kopia[i] = this.tab[i];
        }

        // nadpisuję starą tablicę nową tablicą
        this.tab = kopia;
    }

    public int size() {
        return this.size;
        // nie jest odporne na wstawianie null'i w tablicę
//        for (int i = 0; i < tab.length; i++) {
//            if(tab[i] == null){
//                // jeśli znalazłem null, to znaczy że w tej komórce w tablicy jest pusto
//                return i;
//            }
//        }
//
//        return tab.length;
    }

    public Object get(int indeks) {
        return this.tab[indeks];
    }

    public int indexOf(Object o) {
        // iteruję od początku listy aż do jej rozmiaru
        for (int i = 0; i < this.size; i++) {
//            if(null.equals(o)){
//            if(tab[i].equals(o)){
            // jeśli znalazłem element szukany, to zwracam jego indeks
            if (tab[i] == o) {
                return i;
            }
        }
        // jeśli w pętli nie udało się odnaleźć elementu
        return -1;
    }

    // contains (Object o) - zwraca true/false czy lista posiada dany obiekt
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    // indexOf(Object o) - zwraca indeks elementu na którym występuje element o
    // add(Object... o) - dodaje elementy varargs do listy
    // clear() - czyści tablicę z elementów
    public void clear() {
        this.size = 0;
    }

    /**
     * Dodanie do listy elementów varargs (tablicy)
     *
     * @param tablicaObjects
     */
    public void add(Object... tablicaObjects) {
        for (Object object : tablicaObjects) {
            // używam wcześniej stworzonej metody dodawania 'add)
            add(object);
        }
    }

}
